#include "tree.h"
#include <stdlib.h>


struct tree_node {
  int value;
  struct tree_node *left;
  struct tree_node *right;
};

//Initialise un arbre à NULL
void tree_create(struct tree *self) {
  self->root=NULL;
}

//Detruit tout l'arbre sauf la racine
static void tree_node_destroy(struct tree_node *self){
  if(self!=NULL){
    tree_node_destroy(self->right);
    tree_node_destroy(self->left);
    free(self);
  }
}

//Detruit l'arbre complet
void tree_destroy(struct tree *self) {
  if(self->root==NULL){
    return;
  }
  tree_node_destroy(self->root);
}

//Renvoie true si l'arbre contient une valeur demande (recursif sans la racine)
static bool tree_node_contains(struct tree_node *self, int value){
  if(self==NULL){
    return false;
  }
  if(self->value>value){
    return tree_node_contains(self->left,value);
  }
  if(self->value<value){
    return tree_node_contains(self->right,value);
  }
  return true;
}

//Renvoie true si l'arbre contient une valeur demande
bool tree_contains(const struct tree *self, int value) {
  return tree_node_contains(self->root,value);
}

//Inserer une valeur dans un arbre (recursif sans la racine)
static struct tree_node *tree_node_insert(struct tree_node *self, int value){
  if(self==NULL){
    struct tree_node *nouveau=malloc(sizeof(*nouveau));
    nouveau->left=NULL;
    nouveau->right=NULL;
    nouveau->value=value;
    return nouveau;
  }
  if(value<self->value){
    self->left=tree_node_insert(self->left,value);
  }
  if(value>self->value){
    self->right=tree_node_insert(self->right,value);
  }
  return self;
}

//Inserer une valeur dans un arbre
void tree_insert(struct tree *self, int value) {
  self->root=tree_node_insert(self->root,value);
}

//Supprime des tree_node de maniere recursif
struct tree_node *tree_node_supprime(struct tree_node *self,struct tree_node **supp){
  if(self->left==NULL){
    struct tree_node *droite =self->right;
    self->right=NULL;
    *supp=self;
    return droite;
  }
  self->left=tree_node_supprime(self->left, supp);
  return self;
} 

//Supprime une valeur de l'arbre (recursif sans la racine)
struct tree_node *tree_node_remove(struct tree_node *self, int value){
  if(self==NULL){
    return NULL;
  }
  if(value<self->value){
    self->left=tree_node_remove(self->left, value);
    return self;
  }
  if(value>self->value){
    self->right=tree_node_remove(self->right, value);
    return self;
  }
  struct tree_node *gauche=self->left;
  struct tree_node *droite=self->right;
  free(self);
  self=NULL;
  if(gauche == NULL && droite==NULL){
    return NULL;
  }
  if(gauche==NULL){
    return droite;
  }
  if(droite==NULL){
    return gauche;
  }
  droite=tree_node_supprime(droite, &self);
  self->left = gauche;
  self->right=droite;
  return self;


}

//Supprime une valeur de l'arbre
void tree_remove(struct tree *self, int value) {
  self->root=tree_node_remove(self->root, value);
}

//Renvoi si l'arbre est vide
bool tree_is_empty(const struct tree *self) {
  if(self->root==NULL){
    return true;
  }
  return false;
}

//Renvoi taille de l'arbre (recursif sans racine)
static size_t tree_node_size(struct tree_node *self){
  if(self==NULL){
    return 0;
  }
  size_t res= tree_node_size(self->right)+1+tree_node_size(self->left);
  return res;
}

//Renvoi taille de l'arbre
size_t tree_size(const struct tree *self) {
  return tree_node_size(self->root);
}

//Renvoie la taille de l'arbre (recursif sans racine)
static size_t tree_node_height(struct tree_node *self){
  size_t hauteur_gauche;
  size_t hauteur_droite;
  if(self==NULL){
    return 0;
  }
  hauteur_gauche=tree_node_height(self->left);
  hauteur_droite=tree_node_height(self->right);
  if(hauteur_gauche>=hauteur_droite){
    return 1+hauteur_gauche;
  }
  return 1+hauteur_droite;
}

//Renvoie la taille de l'arbre
size_t tree_height(const struct tree *self) {
  return tree_node_height(self->root); 
}

//Parcours préfixe de l'arbre (recursif sans racine)
void tree_walk_pre_order_recur(struct tree_node *self, tree_func_t func, void *user_data){
  if(self==NULL){
    return;
  }
  func(self->value,user_data);
  tree_walk_pre_order_recur(self->left,func,user_data);
  tree_walk_pre_order_recur(self->right,func,user_data);
}

//Parcours préfixe de l'arbre 
void tree_walk_pre_order(const struct tree *self, tree_func_t func, void *user_data)  {
  tree_walk_pre_order_recur(self->root,func,user_data);
}

//Parcours infixe de l'arbre (recursif sans racine)
void tree_walk_in_order_recur(struct tree_node *self, tree_func_t func, void *user_data){
  if(self==NULL){
    return;
  }
  tree_walk_in_order_recur(self->left,func,user_data);
  func(self->value,user_data);
  tree_walk_in_order_recur(self->right,func,user_data);
}

//Parcours infixe de l'arbre
void tree_walk_in_order(const struct tree *self, tree_func_t func, void *user_data) {
  tree_walk_in_order_recur(self->root,func,user_data);
}

//Parcours sufixe de l'arbre (recursif sans racine)
void tree_walk_post_order_recur(struct tree_node *self, tree_func_t func, void *user_data){
  if(self==NULL){
    return;
  }
  tree_walk_post_order_recur(self->left,func,user_data);
  tree_walk_post_order_recur(self->right,func,user_data);
  func(self->value,user_data);
}

//Parcours sufixe de l'arbre 
void tree_walk_post_order(const struct tree *self, tree_func_t func, void *user_data) {
  tree_walk_post_order_recur(self->root,func,user_data);
}

//Affiche l'arbre (non-utilisé)
void tree_dump(const struct tree *self) {
  
}
