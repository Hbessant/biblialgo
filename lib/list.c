#include "list.h"

struct list_node {
  int value;
  struct list_node *next;
  // struct list_node *prev;
};

//Initialise une liste à NULL
void list_create(struct list *self) {
	self->first=NULL;
}

//Détruit une liste
void list_destroy(struct list *self) {
	if(self->first==NULL){
		return;
	}
	struct list_node *suppri =malloc(sizeof(*suppri));
	suppri=self->first;
	while(suppri!=NULL){
		struct list_node *supprimer = suppri->next;
		free(suppri);
		suppri=supprimer;
	}
	
		
	
}

//Verifie si une liste est égale à un tableau
bool list_equals(const struct list *self, const int *data, size_t size) {
	struct list_node *element=self->first;
	if (list_size(self)!=size){
		free(element);
		return false;
	}
	while(element!=NULL){
		if (element->value!=*data){
			return false;
		}
		element=element->next;
		data++;
	}
	free(element);
	return true;
	
		
}

//Ajoute un element a la fin de la liste
void list_add_back(struct list *self, int value) {
	
	struct list_node *nouveau=malloc(sizeof(*nouveau));
	nouveau->next=NULL;	
	nouveau->value=value;
	
	
	if (self->first==NULL){
		//struct list_node *test=self->first; // A VERIFIER
		self->first=nouveau;
		return;
	}
	struct list_node *dernier=self->first;
	while(dernier->next){
		dernier=dernier->next;
	}
	dernier->next=nouveau;
	
	
}

//Ajoute un element au debut de la liste
void list_add_front(struct list *self, int value) {
  	struct list_node *node=calloc(1,sizeof(*node));
	node->value=value;
	node->next=self->first;
	self->first=node;
	
}

//Ajoute un element n'importe ou dans le liste
void list_insert(struct list *self, int value, size_t index) {
	struct list_node *nouveau=calloc(1,sizeof(*nouveau));
	nouveau->value=value;
	if(index==0){
		//Ajout au debut
		list_add_front(self,value);
		free(nouveau);
	}else{
		if(index!=list_size(self)){
			//Ajout dans la liste
			index--;
			struct list_node *curr=self->first;
			while(curr->next!=NULL && index!=0){
				curr=curr->next;
				index--;
			}
			nouveau->next=curr->next;
			curr->next=nouveau;
			
		}else{
			//Ajout a la fin
			list_add_back(self,value);
			free(nouveau);
		}
	}
	
}

//Supprime un element de la liste
void list_remove(struct list *self, size_t index) {
	if(index==0){
		struct list_node *remove=self->first;
		self->first=self->first->next;
		free(remove);
	}else{
		if(index!=list_size(self)){
			struct list_node *curr=self->first;
			struct list_node *next=curr->next;
			while(curr->next!=NULL && index!=1){
				curr=curr->next;
				next=next->next;
				index--;
			}
			curr->next=next->next;
			free(next);
			next=curr->next;
		}else{
			struct list_node *curr=self->first;
			struct list_node *next=curr->next;
			while(curr->next!=NULL && index!=1){
				curr=curr->next;
				next=next->next;
				index--;
			}
			curr->next=NULL;
			free(next);
			next=curr->next;
		}
	}
}

//Renvoie le pointeur d'un element de la liste à un certain index
int *list_get(const struct list *self, size_t index) {
	if(self->first==NULL){
		return NULL;
	}
	struct list_node *node=self->first;
	while(index!=0){
		node=node->next;
		index--;
	}
	return &node->value;
		
}

//Verifie si la liste est vide
bool list_is_empty(const struct list *self) {
	if(self->first==NULL){
		return true;
	}
  	return false;
}

//Renvoie la taille de la liste
size_t list_size(const struct list *self) {

	if(self->first==NULL){
		return 0;
	}
	size_t taille=0;
	struct list_node *compte=self->first;
	while(compte!=NULL){
		taille++;
		compte=compte->next;
	}
	
	return taille;
	
}

//Cherche un element dans une liste
size_t list_search(const struct list *self, int value) {
	size_t compteur=0;
	if (self->first==NULL){
		return list_size(self);
	}
  	struct list_node *element=self->first;
  	while(element!=NULL){
  		if (element->value==value){
  			return compteur;
		}
		element=element->next;
		compteur++;
	}
	return list_size(self);
}

//Importe un tableau dans une liste
void list_import(struct list *self, const int *other, size_t size) {
	if(self->first!=NULL){
		list_remove(self,0);
		list_import(self,other,size);
	}else{
		for(int i=0;i<size;i++){
			list_add_back(self,other[i]);
			
		}
		
	}
}

//Affiche la liste
void list_dump(const struct list *self) {
	struct list_node *actu=self->first;
	while(actu!=NULL){
		printf(" %d ->",actu->value);
		actu=actu->next;
	}
	printf(" NULL");
	printf(" \n ");
}

//Dit si la liste est trié
bool list_is_sorted(const struct list *self) {
	struct list_node *parcour=self->first;
	if(list_size(self)<=2){
		return true;
	}
	while(parcour->next!=NULL){
		if(parcour->value > parcour->next->value){
			return false;
		}
		parcour=parcour->next;
	}
	return true;
}

//Transforme la liste dans un tableau
static void list_export(struct list_node *node,int *tab){
	int i=0;
	while(node!=NULL){
		tab[i]=node->value;
		node=node->next;
		i++;
	}
	
}

//Partie du tri fusion
static void array_merge(int *tab, size_t i, size_t milieu, size_t j, int *tmp){
	size_t a=i;
	size_t b=milieu;
	for(size_t k=i;k<j;++k){
		if (a<milieu && (b==j || tab[a]<tab[b])){
			tmp[k]=tab[a];
			a++;
		} else {
			tmp[k]=tab[b];
			b++;
		}
	}
}

//Partie du tri fusion
static void array_merge_sort_partial(int *tab,size_t i,size_t j,int *tmp){
	if (j-i<2){
		return;
	}
	size_t milieu=(i+j)/2;
	array_merge_sort_partial(tab,i,milieu,tmp);
	array_merge_sort_partial(tab,milieu,j,tmp);
	array_merge(tab,i,milieu,j,tmp);
	memcpy(tab + i,tmp+i,(j-i)*sizeof(int));
}

//Effectue un tri fusion sur une liste
void list_merge_sort(struct list *self) {
	if (list_is_sorted(self)==true){
		return;
	}else{
		struct list_node *element=self->first;
		size_t taille=list_size(self);
		int *tab=calloc(taille,sizeof(int));
		list_export(element,tab);	
		int *tmp=calloc(taille,sizeof(int));
		array_merge_sort_partial(tab,0,taille,tmp);
		free(tmp);
		list_import(self,tab,taille);
		free(tab);
	}
		
}
