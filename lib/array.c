#include "array.h"
#include <unistd.h>




//Initialise un array vide
void array_create(struct array *self) {
	self->size=0;
	self->capacity=100;
	self->data=malloc(self->capacity *sizeof(int));
	
}

//Detruit un array
void array_destroy(struct array *self) {
	free(self->data);
}

//Renvoie si les 2 tableaux ont la meme taille et le meme contenu
bool array_equals(const struct array *self, const int *content, size_t size) {
	if(self->size!=size){
		return false;
	}
	for(int i=0;i<size;i++){
		if(self->data[i]!=content[i]){
			return false;
		}
	}
	return true;
}

//Augmente la capacité du tableau (x2)
static void growth_capacity_array(struct array *self){
	self->capacity=(self->capacity)*2;
	int *newdata=calloc(self->capacity,sizeof(int));	//Creer nouveau tableau avec taille 2* plus grande si plus de place
	memcpy(newdata,self->data,self->size*sizeof(int));
	free(self->data);
	self->data=newdata;
}


//Ajoute un élément à la fin du tableau
void array_add(struct array *self, int value) {
	if(self->size==self->capacity){
		growth_capacity_array(self);
		
	}
	self->data[self->size]=value; //Ajoute à la fin
	self->size++;
}

//Ajoute un élément au début du tableau
static void array_add_beginning(struct array *self, int value) {
	if(self->size==self->capacity){
		growth_capacity_array(self);
		
	}
	int i=self->size;
	for(i=self->size;i>0;i--){
		self->data[i]=self->data[i-1];
	}
	self->data[i]=value;	//Ajoute au début
	self->size++;
	
	
}

//Ajoute un élément au milieu du tableau
static void array_add_middle(struct array *self, int value, size_t index) {
	if(self->size==self->capacity){
		growth_capacity_array(self);
		
	}
	int i=self->size;
	for(i=self->size;i>index;i--){
		self->data[i]=self->data[i-1];
	}
	self->data[i]=value;	//Ajoute au milieu
	self->size++;
	
	
}

//Insérer élèment dans tableau
void array_insert(struct array *self, int value, size_t index) {
	if(array_is_empty(self)==true){
		self->data[0]=value;
		self->size+=1;
	}else{
		if(self->size==index){
			array_add(self,value);
		}else{
			if(index==0){
				array_add_beginning(self,value);
			}else{
				array_add_middle(self,value,index);
			}
		}

	}
}

//Supprime un élèment du tableau et conserve l'ordre
void array_remove(struct array *self, size_t index) {
	if(index==self->size-1){
		self->data[index]=0;
	}else{
		self->data[index]=0;
		int i=index;
		for(i=index;i<self->size-1;i++){
			self->data[i]=self->data[i+1];
		}
		self->data[i]=0;
	}
	self->size-=1;
}

//Retourne le pointeur d'un certain index du tableau
int *array_get(const struct array *self, size_t index) {
	if(self->size==0){
		return NULL;
	}else{
		if(index<self->size){
			return &self->data[index];
		}
		return NULL;
	}
	
}
//Verifie si le tableau est vide
bool array_is_empty(const struct array *self) {
	if(self->size==0){
		return true;
	}
	return false;

}

//Retourne le nombre d'élèments contenus dans le tableau
size_t array_size(const struct array *self) {
	return self->size;
}

//Cherche une valeur dans le tableau
size_t array_search(const struct array *self, int value) {
	if(array_is_empty(self)==true){
		return 0;
	}
	size_t i=0;
	for(i=0;i<self->size;i++){
		if(self->data[i]==value){
			return i;
		}
	}
	return self->size;	//Renvoie la taille du tableau si ppas de présence de value
}
//Recherche une valeur de manière dichotomique dans un tableau trié
size_t array_search_sorted(const struct array *self, int value) {
	int min=self->data[0];
	int max=self->data[self->size-1];
	size_t milieu=0;
	while(min<max){
		milieu=(min+max)/2;
		if(self->data[self->size-1]<value){
			return self->size;
		}
		if(self->data[milieu]==value){
			return milieu;
		}else{
			if(self->data[milieu]<value){
				min=milieu+1;
			}else{
				max=milieu-1;
			}
		}
	}
	if(self->data[min]==value){
		return min;
	}else{
		return self->size;	//Renvoie la taille du tableau si ppas de présence de value
	}
  
}

//Importe un nouveau tableau dans celui de base
void array_import(struct array *self, const int *other, size_t size) {
	int i=0;
	if(self->size>size){
		while(i!=size){
			self->data[i]=other[i];
			++i;
		}
		while(i!=self->size){
			self->data[i]=0;
			self->size-=1;
		}
	}else{
		while(i!=size){
			if(size>self->capacity){
				growth_capacity_array(self);
			}
			self->data[i]=other[i];
			++i;
		}
		self->size=self->size+(size-self->size);
	}
	
	

}

void array_dump(const struct array *self) {

}

//Dit si le tableau est trié
bool array_is_sorted(const struct array *self) {
	for(size_t i=1;i<self->size;++i){
		if(self->data[i-1]>self->data[i]){
			return false;
		}
	}
  	return true;
}

static void struct_array_swap(struct array *self, size_t i, size_t j){
	int inter=self->data[i];
	self->data[i]=self->data[j];
	self->data[j]=inter;

}

//Tri le tableau grâce à un tri par Sélection
void array_selection_sort(struct array *self) {
	for(size_t i=0;i<self->size-1;++i){
		size_t j=i;
		for(size_t k=j+1;k<self->size;++k){
			if(self->data[k]<self->data[j]){
				j=k;
			}
		}
		struct_array_swap(self,i,j);
	}
}

//Tri le tableau grâce à un tri à bulle
void array_bubble_sort(struct array *self) {
	for(size_t i=0;i<self->size-1;++i){
		for(size_t j=self->size-1;j>i;--j){
			if(self->data[j]<self->data[j-1]){
				struct_array_swap(self,j,j-1);
			}
		}
	}
}

//Tri le tableau grâce à un tri par insertion
void array_insertion_sort(struct array *self) {
	for(size_t i=1;i<self->size;++i){
		int inter=self->data[i];
		size_t j=i;
		while(j>0 && self->data[j-1]>inter){
			self->data[j]=self->data[j-1];
			j--;
		}
		self->data[j]=inter;
	}
}


//Partitionne les valeurs entre 2 indices
static ssize_t array_partition(struct array *self,ssize_t i,ssize_t j){
	ssize_t pivot_i=i;
	const int pivot=self->data[pivot_i];
	struct_array_swap(self,pivot_i,j);
	ssize_t l=i;
	for(ssize_t k=i;k<j;++k){
		if(self->data[k]<pivot){
			struct_array_swap(self,k,l);
			l++;
		}
	}
	struct_array_swap(self,l,j);
	return l;
}


//Tri rapide partiellement entre 2 indices
static void array_quick_sort_partial(struct array *self,ssize_t i, ssize_t j){
	if(i<j){
		ssize_t p=array_partition(self,i,j);
		array_quick_sort_partial(self,i,p-1);
		array_quick_sort_partial(self,p+1,j);
	}
}


//Tri le tableau grâce à un tri rapide
void array_quick_sort(struct array *self) {
	array_quick_sort_partial(self,0,self->size-1);
}

//Intervertit 2 valeurs
static void array_swap(int *data, size_t i, size_t j){
	int inter=data[i];
	data[i]=data[j];
	data[j]=inter;

}

//Insertion d'un element dans un tas
static void heap_insert(int *heap, size_t n, int value){
	size_t i=n;
	heap[i]=value;
	while(i>0){
		ssize_t j=(i-1)/2;
		if (heap[i]<heap[j]){
			break;
		}
		array_swap(heap,i,j);
		i=j;
	}
}

//Supprime le sommet d'un tas
static void heap_remove(int *heap,size_t n){
	heap[0]=heap[n-1];
	size_t i=0;
	while(i<(n-1)/2){
		size_t lt =2*i+1;
		size_t rt =2*i+2;
		if(heap[i]>heap[lt] && heap[i]> heap[rt]){
			break;
		}
		size_t j=(heap[lt] > heap[rt]) ? lt : rt;
		array_swap(heap, i,j);
		i=j;
	}
}

//Tri le tableau grâce à un tri par tas
void array_heap_sort(struct array *self) {
	for(size_t i=0; i<self->size;++i){
		int value = self->data[i];
		heap_insert(self->data,i,value);
	}
	for (size_t j=0;j<self->size;++j){
		int value=self->data[0];
		heap_remove(self->data,self->size-j);
		self->data[self->size-j-1]=value;
	}
}

//Dit si le tableau est un tas
bool array_is_heap(const struct array *self) {
	if (self->size==0){
		return true;
	}
	if(self->size==1){
		return true;
	}
	for(int i=1;i<self->size;i++){
		if(self->data[i]>self->data[(i-1)/2]){
			return false;
		}
	}
	return true;
}

//Ajoute une valeur à un tableau considéré comme un tas
void array_heap_add(struct array *self, int value) {
	if(self->size==self->capacity){
		growth_capacity_array(self);
	}
	heap_insert(self->data,self->size,value);
	self->size+=1;

}

//Renvoi la valeur au dessus du tas
int array_heap_top(const struct array *self) {
  return self->data[0];
}

//Supprime la valeur du dessus du tas
void array_heap_remove_top(struct array *self) {
	if(self->size>0){
		heap_remove(self->data,self->size);
		self->size-=1;
	}
}
